
package sc;

import jtps.jTPS_Transaction;
import sc.data.Slide;
public class update_Transaction implements jTPS_Transaction{
    String captionTextField; 
    double currentHeightSlider; 
    double currentWidthSlider; 
    double xPos; 
    double yPos;
    Slide slide;
    String captionTextField1; 
    double currentHeightSlider1; 
    double currentWidthSlider1; 
    double xPos1; 
    double yPos1;
    public update_Transaction(Slide slide,String captionTextField, double currentHeightSlider, 
            double currentWidthSlider, double xPos, double yPos){
        this.captionTextField = captionTextField;
        this.currentHeightSlider = currentHeightSlider;
        this.currentWidthSlider=currentWidthSlider;
        this.xPos = xPos;
        this.yPos = yPos;
        this.slide = slide;
        captionTextField1 = slide.getCaption();
        currentHeightSlider1 = slide.getCurrentHeight();
        currentWidthSlider1 = slide.getCurrentWidth();
        xPos1 = slide.getX();
        yPos1 = slide.getY();
    }
    
    @Override
    public void doTransaction(){
            slide.setCaption(captionTextField);
            Integer height = (int) currentHeightSlider;
            slide.setCurrentHeight(height);
            Integer width = (int) currentWidthSlider;
            slide.setCurrentWidth(width);
            Integer x = (int) xPos;
            slide.setX(x);
            Integer y = (int) yPos;
            slide.setY(y);
    };
    @Override
    public void undoTransaction(){
            slide.setCaption(captionTextField1);
            Integer height = (int) currentHeightSlider1;
            slide.setCurrentHeight(height);
            Integer width = (int) currentWidthSlider1;
            slide.setCurrentWidth(width);
            Integer x = (int) xPos1;
            slide.setX(x);
            Integer y = (int) yPos1;
            slide.setY(y);
    };
}
