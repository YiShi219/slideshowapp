package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import properties_manager.PropertiesManager;
import sc.SlideshowCreatorApp;
import static sc.SlideshowCreatorProp.APP_PATH_WORK;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_MESSAGE;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_TITLE;
import sc.data.SlideshowCreatorData;
import javafx.stage.FileChooser;
import jtps.jTPS;

import sc.data.Slide;
import sc.update_Transaction;


/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class SlideshowCreatorController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    SlideshowCreatorApp app;
    jTPS jTPS = new jTPS();
    boolean added = false;
    List<Slide> myList = new ArrayList<Slide>();
    /**
     * Constructor, note that the app must already be constructed.
     */
    public SlideshowCreatorController(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    // CONTROLLER METHOD THAT HANDLES ADDING A DIRECTORY OF IMAGES
    public void handleAddAllImagesInDirectory() {
        added = false;
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        try {
            // ASK THE USER TO SELECT A DIRECTORY
            DirectoryChooser dirChooser = new DirectoryChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            dirChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File dir = dirChooser.showDialog(app.getGUI().getWindow());
            if (dir != null) {
                File[] files = dir.listFiles();
                for (File f : files) {
                    String fileName = f.getName();
                    if (fileName.toLowerCase().endsWith(".png") ||
                            fileName.toLowerCase().endsWith(".jpg") ||
                            fileName.toLowerCase().endsWith(".gif")) {
                        String path = f.getPath();
                        String caption = "";
                        Image slideShowImage = loadImage(path);
                        int originalWidth = (int)slideShowImage.getWidth();
                        int originalHeight = (int)slideShowImage.getHeight();
                        
                        Slide slide = new Slide(fileName, path, caption, originalWidth, originalHeight);
                        myList.add(slide);
                        //added = data.added;
                    }
                }
                for (int i = 0, k = 1; i < myList.size(); i++, k++){
                    for (int j = k; j < myList.size(); j++){
                        if (myList.get(i).getFileName().equals(myList.get(j).getFileName())){
                            myList.remove(i);
                        }
                    }
                }
                if (myList.size() != 0){
                    added = true;
                }
                data.addSlide(myList);
            }
        }
        catch(MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);
        }
        myList.clear();
    }
    
    public void handleAddImage(){
        added = false;
        try{
            FileChooser fc = new FileChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            fc.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File dir = fc.showOpenDialog(app.getGUI().getWindow());
             if (dir != null) {
                String fileName = dir.getName();
                if (fileName.toLowerCase().endsWith(".png") ||
                                fileName.toLowerCase().endsWith(".jpg") ||
                                fileName.toLowerCase().endsWith(".gif")) {
                    String path = dir.getPath();
                    Image slideShowImage = loadImage(path);
                    String caption = "";
                    int originalWidth = (int)slideShowImage.getWidth();
                    int originalHeight = (int)slideShowImage.getHeight();
                    SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
                    data.addSlide(fileName, path, caption, originalWidth, originalHeight);
                    //SlideshowCreatorData.addImage_Transaction transaction = data.new addImage_Transaction(data.getSlides(),fileName,path,caption,originalWidth,originalHeight);
                    //jTPS.addTransaction(transaction);
                    added = data.added;
                }
            }
        }catch(MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);
        }
    }
    
    public void handleRemoveImage(String path){   
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        data.removeSlide(path);
    }
    
    public void handleUpdate(Slide slide,String captionTextField, double currentHeightSlider, double currentWidthSlider, double xPos, double yPos){
        if (slide != null){
            update_Transaction transaction = new update_Transaction(slide,captionTextField,currentHeightSlider,currentWidthSlider,xPos,yPos);
            jTPS.addTransaction(transaction);
        }
    }
    
    
    // THIS HELPER METHOD LOADS AN IMAGE SO WE CAN SEE IT'S SIZE
    private Image loadImage(String imagePath) throws MalformedURLException {
	File file = new File(imagePath);
	URL fileURL = file.toURI().toURL();
	Image image = new Image(fileURL.toExternalForm());
	return image;
    }
}