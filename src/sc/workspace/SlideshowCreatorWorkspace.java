package sc.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppPropertyType.APP_LOGO;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import sc.SlideshowCreatorApp;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import static sc.SlideshowCreatorProp.ADD_ALL_IMAGES_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.ADD_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.CAPTION_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_HEIGHT_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_HEIGHT_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_WIDTH_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_WIDTH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.FILE_NAME_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.FILE_NAME_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.NEXT_BUTTON_ICON;
import static sc.SlideshowCreatorProp.ORIGINAL_HEIGHT_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.ORIGINAL_WIDTH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.PATH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.PLAYANDPAUSE_BUTTON_ICON;
import static sc.SlideshowCreatorProp.PREVIOUS_BUTTON_ICON;
import static sc.SlideshowCreatorProp.REDO_ICON;
import static sc.SlideshowCreatorProp.REMOVE_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.SLIDEDOWN_ICON;
import static sc.SlideshowCreatorProp.SLIDEUP_ICON;
import static sc.SlideshowCreatorProp.UNDO_ICON;
import static sc.SlideshowCreatorProp.UPDATE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.XPOSITION_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.XPOSITION_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.YPOSITION_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.YPOSITION_PROMPT_TEXT;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_BUTTON;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_SLIDER;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_TEXT_FIELD;
import static sc.style.SlideshowCreatorStyle.CLASS_PROMPT_LABEL;
import static sc.style.SlideshowCreatorStyle.CLASS_SLIDES_TABLE;
import static sc.style.SlideshowCreatorStyle.CLASS_UPDATE_BUTTON;

/**
 * This class serves as the workspace component for the TA Manager
 * application. It provides all the user interface controls in 
 * the workspace area.
 * 
 * @author Richard McKenna
 */
public class SlideshowCreatorWorkspace extends AppWorkspaceComponent {
    jTPS jTPS = new jTPS();
    //String SlideShowTitle = "";
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    SlideshowCreatorApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    SlideshowCreatorController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editImagesToolbar;
    Button addAllImagesInDirectoryButton;
    Button addImageButton;
    Button removeImageButton;
    HBox TitleToolBar;
    Button SlideUp;
    Button SlideDown;
    Button Undo;
    Button Redo;
    
    // FOR THE SLIDES TABLE
    ScrollPane slidesTableScrollPane;
    TableView<Slide> slidesTableView;
    TableColumn<Slide, StringProperty> fileNameColumn;
    TableColumn<Slide, IntegerProperty> currentWidthColumn;
    TableColumn<Slide, IntegerProperty> currentHeightColumn;
    TableColumn<Slide, IntegerProperty> xPositionColumn;
    TableColumn<Slide, IntegerProperty> yPositionColumn;
    

    // THE EDIT PANE
    GridPane editPane;
    Label fileNamePromptLabel;
    TextField fileNameTextField;
    Label pathPromptLabel;
    TextField pathTextField;
    Label captionPromptLabel;
    TextField captionTextField;
    Label originalWidthPromptLabel;
    TextField originalWidthTextField;
    Label originalHeightPromptLabel;
    TextField originalHeightTextField;
    Label currentWidthPromptLabel;
    Slider currentWidthSlider;
    Label currentHeightPromptLabel;
    Slider currentHeightSlider;
    Button updateButton;
    TextField Title;
    Slider xPosSlider;
    Slider yPosSlider;  
    Label xPosPromptLabel;
    Label yPosPromptLabel;
    
    /**
     * The constructor initializes the user interface for the
     * workspace area of the application.
     */
    public SlideshowCreatorWorkspace(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }
    
    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // FIRST MAKE ALL THE COMPONENTS
        editImagesToolbar = new HBox();
        addAllImagesInDirectoryButton = new Button(props.getProperty(ADD_ALL_IMAGES_BUTTON_TEXT));
        addImageButton = new Button(props.getProperty(ADD_IMAGE_BUTTON_TEXT));
        removeImageButton = new Button(props.getProperty(REMOVE_IMAGE_BUTTON_TEXT));
        slidesTableScrollPane = new ScrollPane();
        slidesTableView = new TableView();
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT));
        currentWidthColumn = new TableColumn(props.getProperty(CURRENT_WIDTH_COLUMN_TEXT));
        currentHeightColumn = new TableColumn(props.getProperty(CURRENT_HEIGHT_COLUMN_TEXT));
        xPositionColumn = new TableColumn(props.getProperty(XPOSITION_COLUMN_TEXT));
        yPositionColumn = new TableColumn(props.getProperty(YPOSITION_COLUMN_TEXT));
        editPane = new GridPane();
        fileNamePromptLabel = new Label(props.getProperty(FILE_NAME_PROMPT_TEXT));
        fileNameTextField = new TextField();
        pathPromptLabel = new Label(props.getProperty(PATH_PROMPT_TEXT));
        pathTextField = new TextField();
        captionPromptLabel = new Label(props.getProperty(CAPTION_PROMPT_TEXT));
        captionTextField = new TextField();
        originalWidthPromptLabel = new Label(props.getProperty(ORIGINAL_WIDTH_PROMPT_TEXT));
        originalWidthTextField = new TextField();
        originalHeightPromptLabel = new Label(props.getProperty(ORIGINAL_HEIGHT_PROMPT_TEXT));
        originalHeightTextField = new TextField();
        currentWidthPromptLabel = new Label(props.getProperty(CURRENT_WIDTH_PROMPT_TEXT));
        currentWidthSlider = new Slider(0,1000,0);
        currentWidthSlider.setMajorTickUnit(200);
        currentWidthSlider.setMinorTickCount(9);
        currentWidthSlider.setShowTickLabels(true);
        currentWidthSlider.setShowTickMarks(true);
        currentHeightPromptLabel = new Label(props.getProperty(CURRENT_HEIGHT_PROMPT_TEXT));
        currentHeightSlider = new Slider(0,1000,0);
        currentHeightSlider.setMajorTickUnit(200);
        currentHeightSlider.setMinorTickCount(9);
        currentHeightSlider.setShowTickLabels(true);
        currentHeightSlider.setShowTickMarks(true);
        xPosSlider = new Slider(0,1000,0);
        xPosSlider.setMajorTickUnit(200);
        xPosSlider.setMinorTickCount(9);
        xPosSlider.setShowTickLabels(true);
        xPosSlider.setShowTickMarks(true);
        yPosSlider = new Slider(0,1000,0);
        yPosSlider.setMajorTickUnit(200);
        yPosSlider.setMinorTickCount(9);
        yPosSlider.setShowTickLabels(true);
        yPosSlider.setShowTickMarks(true);
        xPosPromptLabel = new Label(props.getProperty(XPOSITION_PROMPT_TEXT));
        yPosPromptLabel = new Label(props.getProperty(YPOSITION_PROMPT_TEXT));
        updateButton = new Button(props.getProperty(UPDATE_BUTTON_TEXT));
        
        Title = new TextField();
        TitleToolBar = new HBox();
        Title.setPromptText("Slideshow Title");
        
        
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(SLIDEUP_ICON.toString()); 
        Image upImage = new Image(imagePath);
        SlideUp = new Button();
        SlideUp.setGraphic(new ImageView(upImage));
        
        String imagePath2 = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(SLIDEDOWN_ICON.toString()); 
        Image downImage = new Image(imagePath2);
        SlideDown = new Button();
        SlideDown.setGraphic(new ImageView(downImage));
        
        String imagePath3 = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(UNDO_ICON.toString()); 
        Image UndoImage = new Image(imagePath3);
        Undo = new Button();
        Undo.setGraphic(new ImageView(UndoImage));
        
        String imagePath4 = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REDO_ICON.toString()); 
        Image RedoImage = new Image(imagePath4);
        Redo = new Button();
        Redo.setGraphic(new ImageView(RedoImage));
        
        
        
               
        // ARRANGE THE TABLE
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT));
        currentWidthColumn = new TableColumn(props.getProperty(CURRENT_WIDTH_COLUMN_TEXT));
        currentHeightColumn = new TableColumn(props.getProperty(CURRENT_HEIGHT_COLUMN_TEXT));
        //xPositionColumn = new TableColumn(props.getProperty(XPOSITION_COLUMN_TEXT));
        //yPositionColumn = new TableColumn(props.getProperty(YPOSITION_COLUMN_TEXT));
        slidesTableView.getColumns().add(fileNameColumn);
        slidesTableView.getColumns().add(currentWidthColumn);
        slidesTableView.getColumns().add(currentHeightColumn);
        slidesTableView.getColumns().add(xPositionColumn);
        slidesTableView.getColumns().add(yPositionColumn);
        fileNameColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(3));
        currentWidthColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(6));
        currentHeightColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(6));
        xPositionColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(6));
        yPositionColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(6));
        fileNameColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );
        currentWidthColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("currentWidth")
        );
        currentHeightColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("CurrentHeight")
        );
        xPositionColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("x")
        );
        yPositionColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("y")
        );
        // HOOK UP THE TABLE TO THE DATA
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        ObservableList<Slide> model = data.getSlides();
        slidesTableView.setItems(model);
        
        
        
        // THEM ORGANIZE THEM
        
        TitleToolBar.getChildren().add(Title);
        editImagesToolbar.getChildren().add(addAllImagesInDirectoryButton);
        editImagesToolbar.getChildren().add(addImageButton);
        editImagesToolbar.getChildren().add(removeImageButton);
        editImagesToolbar.getChildren().add(SlideUp);
        editImagesToolbar.getChildren().add(SlideDown);
        editImagesToolbar.getChildren().add(Undo);
        editImagesToolbar.getChildren().add(Redo);
        slidesTableScrollPane.setContent(slidesTableView);
        editPane.add(fileNamePromptLabel, 0, 0);
        editPane.add(fileNameTextField, 1, 0);
        editPane.add(pathPromptLabel, 0, 1);
        editPane.add(pathTextField, 1, 1);
        editPane.add(captionPromptLabel, 0, 2);
        editPane.add(captionTextField, 1, 2);
        editPane.add(originalWidthPromptLabel, 0, 3);
        editPane.add(originalWidthTextField, 1, 3);
        editPane.add(originalHeightPromptLabel, 0, 4);
        editPane.add(originalHeightTextField, 1, 4);
        editPane.add(currentWidthPromptLabel, 0, 5);
        editPane.add(currentWidthSlider, 1, 5);
        editPane.add(currentHeightPromptLabel, 0, 6);
        editPane.add(currentHeightSlider, 1, 6);
        editPane.add(xPosSlider, 1, 7);
        editPane.add(yPosSlider, 1, 8);
        editPane.add(xPosPromptLabel, 0, 7);
        editPane.add(yPosPromptLabel, 0, 8);
        editPane.add(updateButton, 0, 9);
        
        // DISABLE THE DISPLAY TEXT FIELDS
        fileNameTextField.setDisable(true);
        pathTextField.setDisable(true);
        originalWidthTextField.setDisable(true);
        originalHeightTextField.setDisable(true);
        Title.setDisable(true);
        
        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        app.getGUI().getTopToolbarPane().getChildren().add(TitleToolBar);
        app.getGUI().getTopToolbarPane().getChildren().add(editImagesToolbar);
        BorderPane workspaceBorderPane = new BorderPane();
        workspaceBorderPane.setCenter(slidesTableScrollPane);
        slidesTableScrollPane.setFitToWidth(true);
        slidesTableScrollPane.setFitToHeight(true);
        workspaceBorderPane.setRight(editPane);
        
        // AND SET THIS AS THE WORKSPACE PANE
        workspace = workspaceBorderPane;
       
        
        //*****************************************
        captionTextField.setDisable(true);
        addAllImagesInDirectoryButton.setDisable(true);
        addImageButton.setDisable(true);
        removeImageButton.setDisable(true);
        updateButton.setDisable(true);
        SlideUp.setDisable(true);
        SlideDown.setDisable(true);
        Undo.setDisable(true);
        Redo.setDisable(true);
    }
    public void updateWorkSpaceButton(boolean a){
        addAllImagesInDirectoryButton.setDisable(a);
        addImageButton.setDisable(a);
        Title.setDisable(a);
        SlideUp.setDisable(true);
        SlideDown.setDisable(true);
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        Title.setText(data.getText());
        
    }
    public void updateRedoButton(){
        Redo.setDisable(true);
    }
    
    
    //*****************************************
    
    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new SlideshowCreatorController(app);

        addAllImagesInDirectoryButton.setOnAction(e->{           
            controller.handleAddAllImagesInDirectory();
            slidesTableView.getSelectionModel().clearSelection();
            deselect();
            updateButton.setDisable(true);
            removeImageButton.setDisable(true);          
            if(controller.added){
                if (app.getGUI().getFileController().isSaved())
                    app.getGUI().updateSaveButton(false);
                app.getGUI().getFileController().markFileAsNotSaved();  
                app.getGUI().updateToolbarControls(false);
                Undo.setDisable(false);
                app.getGUI().updateViewButton(false);
            }    
        });
        
        addImageButton.setOnAction(e->{
            controller.handleAddImage();
            if (controller.added){
                if (app.getGUI().getFileController().isSaved())
                    app.getGUI().updateSaveButton(false);
                slidesTableView.getSelectionModel().selectLast();
                loadInfo();
                app.getGUI().updateToolbarControls(false);
                app.getGUI().getFileController().markFileAsNotSaved();
                Undo.setDisable(false);
                app.getGUI().updateViewButton(false);
            }
            updateButton.setDisable(true);
            SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        });
        
        removeImageButton.setOnAction(e->{
            if (app.getGUI().getFileController().isSaved())
                app.getGUI().updateSaveButton(false);
            Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
            if (slide != null){
                String path = slide.getPath();
                controller.handleRemoveImage(path);
                slidesTableView.getSelectionModel().clearSelection();
                deselect();
                updateButton.setDisable(true);
                removeImageButton.setDisable(true);
                app.getGUI().updateToolbarControls(false);
                app.getGUI().getFileController().markFileAsNotSaved();
                SlideUp.setDisable(true);
                SlideDown.setDisable(true);
                Undo.setDisable(false);
            }
            if (app.getDataComponent().noSlide()){
                app.getGUI().updateViewButton(true);
            }
            //SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
            //System.out.println(data.getText());
        });
        
        updateButton.setOnAction(e->{
            if (app.getGUI().getFileController().isSaved())
                app.getGUI().updateSaveButton(false);
            Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
            if (slide != null){
                controller.handleUpdate(slide,captionTextField.getText(),currentHeightSlider.getValue(),currentWidthSlider.getValue(),
                xPosSlider.getValue(),yPosSlider.getValue());
                slidesTableView.refresh();
                updateButton.setDisable(true);
                app.getGUI().updateToolbarControls(false);
                app.getGUI().getFileController().markFileAsNotSaved();
                Undo.setDisable(false);
            }
            SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
            if (!data.getText().equals(Title.getText())){
                //data.setText(Title.getText());
                title_Transaction transaction = new title_Transaction(Title.getText());
                jTPS.addTransaction(transaction);
                updateButton.setDisable(true);
                app.getGUI().updateToolbarControls(false);
                app.getGUI().getFileController().markFileAsNotSaved();
                Undo.setDisable(false);
            }
        });
        
        slidesTableView.setOnMouseClicked(e->{
            loadInfo();
        });
        
        currentHeightSlider.setOnMouseClicked(x->{
            Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
            if (slide != null && slide.getCurrentHeight() != currentHeightSlider.getValue())
                updateButton.setDisable(false);
        });
        
        currentWidthSlider.setOnMouseClicked(x->{
            Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
            if (slide != null && slide.getCurrentWidth() != currentWidthSlider.getValue())
                updateButton.setDisable(false);
        });
        
        captionTextField.setOnKeyPressed(x->{
            Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
            if (slide != null && !slide.getCaption().equals(captionTextField.getText()))
                updateButton.setDisable(false);
        });
        Title.setOnKeyPressed(x->{
            SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
           if (!data.getText().equals(Title.getText())){
               updateButton.setDisable(false);
           }
        });
        
        SlideUp.setOnAction(x->{
            if (app.getGUI().getFileController().isSaved())
                app.getGUI().updateSaveButton(false);
            Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
            SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
            Slide slide2 = data.getSlides().get(data.getSlides().indexOf(slide)-1);
            if (slide != null){
                //swap(slide,slide2);
                slideUp_Transaction transaction = new slideUp_Transaction(slide2,slide);
                jTPS.addTransaction(transaction);
                slidesTableView.refresh();   
                slidesTableView.getSelectionModel().select(slide);
                if (slide.equals(data.getSlides().get(0)))
                    SlideUp.setDisable(true);
                else
                    SlideUp.setDisable(false);
                if (slide.equals(data.getSlides().get(data.getSlides().size()-1)))
                    SlideDown.setDisable(true);
                else
                    SlideDown.setDisable(false);
                app.getGUI().updateToolbarControls(false);
                app.getGUI().getFileController().markFileAsNotSaved();
                Undo.setDisable(false);
                Redo.setDisable(true);
            }
        });
        
        SlideDown.setOnAction(x->{
            if (app.getGUI().getFileController().isSaved())
                app.getGUI().updateSaveButton(false);
            Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
            SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
            Slide slide1 = data.getSlides().get(data.getSlides().indexOf(slide)+1);
            if (slide != null){
                //swapDown(slide1,slide);
                slideUp_Transaction transaction = new slideUp_Transaction(slide,slide1);
                jTPS.addTransaction(transaction);
                slidesTableView.refresh(); 
                slidesTableView.getSelectionModel().select(slide);
                if (slide.equals(data.getSlides().get(0)))
                    SlideUp.setDisable(true);
                else
                    SlideUp.setDisable(false);
                if (slide.equals(data.getSlides().get(data.getSlides().size()-1)))
                    SlideDown.setDisable(true);
                else
                    SlideDown.setDisable(false);
                app.getGUI().updateToolbarControls(false);
                app.getGUI().getFileController().markFileAsNotSaved();
                Undo.setDisable(false);
                Redo.setDisable(true);
            }
        });
        
        xPosSlider.setOnMouseClicked(x->{
            Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
            if (slide != null){
                if (xPosSlider.getValue() != slide.getX()){
                    updateButton.setDisable(false);
                }   
            }
        });
        
        yPosSlider.setOnMouseClicked(x->{
            Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
            if (slide != null){
                if (yPosSlider.getValue() != slide.getY()){
                    updateButton.setDisable(false);
                }   
            }
        });
        
        Undo.setOnAction(x->{
            undo();
        });
        
        Redo.setOnAction(x->{
           redo();
        });       
        app.getGUI().getPrimaryScene().setOnKeyPressed(e->{
            if (e.getCode() == KeyCode.Y && e.isControlDown()){
                if (!Redo.isDisabled())
                    redo();
            }else if(e.getCode() == KeyCode.Z && e.isControlDown()){
                if (!Undo.isDisabled())
                    undo();
            }           
        });
        
        
    }
    public void undo(){
        
        jTPS.undoTransaction();
            Redo.setDisable(false);
            if (jTPS.getMostRecentTransactions() == -1){
                Undo.setDisable(true);             
            }
            slidesTableView.getSelectionModel().clearSelection();
            deselect();
            slidesTableView.refresh();
            if (app.getDataComponent().noSlide()){
            app.getGUI().updateViewButton(true);
            }else
            app.getGUI().updateViewButton(false);
    }
    public void redo(){
        
        Undo.setDisable(false);
           jTPS.doTransaction();
           if (jTPS.getTransactions().size() == jTPS.getMostRecentTransactions()+1){
                Redo.setDisable(true);               
           }
           deselect();
           slidesTableView.refresh();
           if (app.getDataComponent().noSlide()){
            app.getGUI().updateViewButton(true);
            }else
            app.getGUI().updateViewButton(false);
    }
    
    //
    private void loadInfo(){
        if (editPane.getChildren().size() > 19)
            editPane.getChildren().remove(19);
        Slide slide = slidesTableView.getSelectionModel().getSelectedItem();
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        if (slide != null){
            captionTextField.setDisable(false);
            captionTextField.setText(slide.getCaption());
            originalHeightTextField.setText("" + slide.getOriginalHeight());
            originalWidthTextField.setText("" + slide.getOriginalWidth());
            fileNameTextField.setText(slide.getFileName());
            pathTextField.setText(slide.getPath());
            Integer newHeight = slide.getCurrentHeight();
            currentHeightSlider.setValue(newHeight);
            Integer newWidth = slide.getCurrentWidth();
            currentWidthSlider.setValue(newWidth); 
            removeImageButton.setDisable(false);
            if (slide.equals(data.getSlides().get(0)))
                SlideUp.setDisable(true);
            else
                SlideUp.setDisable(false);
            if (slide.equals(data.getSlides().get(data.getSlides().size()-1)))
                SlideDown.setDisable(true);
            else
                SlideDown.setDisable(false);
            xPosSlider.setValue(slide.getX());
            yPosSlider.setValue(slide.getY());
            Image image = new Image("file:"+slide.getPath(),150,150,false,false);
            ImageView iv = new ImageView();
            iv.setImage(image);
            editPane.add(iv, 1, 10);   
            
        }          
            updateButton.setDisable(true);
    }        
    
    private void deselect(){
        captionTextField.setDisable(true);
        captionTextField.setText(null);
        originalHeightTextField.setText(null);
        originalWidthTextField.setText(null);
        fileNameTextField.setText(null);
        pathTextField.setText(null);
        currentHeightSlider.setValue(0);
        currentWidthSlider.setValue(0);
        xPosSlider.setValue(0);
        yPosSlider.setValue(0);
        if (editPane.getChildren().size() > 19)
            editPane.getChildren().remove(19);
    }
    
    
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    
    private void initStyle() {
        TitleToolBar.getStyleClass().add(CLASS_BORDERED_PANE);
        editImagesToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        addAllImagesInDirectoryButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        addImageButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeImageButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        slidesTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView.getColumns())
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        
        editPane.getStyleClass().add(CLASS_BORDERED_PANE);
        fileNamePromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fileNameTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        pathPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        pathTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        captionPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        captionTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        originalWidthPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        originalWidthTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        originalHeightPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        originalHeightTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        currentWidthPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentWidthSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        currentHeightPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentHeightSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        updateButton.getStyleClass().add(CLASS_UPDATE_BUTTON);
        Title.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        SlideUp.getStyleClass().add(CLASS_EDIT_BUTTON);
        SlideDown.getStyleClass().add(CLASS_EDIT_BUTTON);
        Undo.getStyleClass().add(CLASS_EDIT_BUTTON);
        Redo.getStyleClass().add(CLASS_EDIT_BUTTON);
        xPosSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        yPosSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        xPosPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        yPosPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
    }

    @Override
    public void resetWorkspace() {
          captionTextField.setDisable(true);
          app.getGUI().updateToolbarControls(false);
          app.getGUI().updateSaveButton(true);
          app.getGUI().updateViewButton(true);
          removeImageButton.setDisable(true);
          updateButton.setDisable(true);
          updateWorkSpaceButton(false);
          fileNameTextField.clear();
          pathTextField.clear();
          captionTextField.clear();
          originalWidthTextField.clear();
          originalHeightTextField.clear();
          currentHeightSlider.setValue(0);
          currentWidthSlider.setValue(0);
          slidesTableView.getSelectionModel().clearSelection();
          Title.clear();
          SlideUp.setDisable(true);
          SlideDown.setDisable(true);
          xPosSlider.setValue(0);
          yPosSlider.setValue(0);
          Undo.setDisable(true);
          Redo.setDisable(true);
          if (editPane.getChildren().size() > 19)
            editPane.getChildren().remove(19);
          jTPS.setTransactions();
          jTPS.setRecentTransaction();
          
    }
    
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        
    }
    
    public void swapDown(Slide x, Slide y){
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        Slide temp = x;
        data.getSlides().set(data.getSlides().indexOf(x), y);
        data.getSlides().set(data.getSlides().indexOf(y),temp);
    }
    public void swapUp(Slide x, Slide y){
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        Slide temp = y;
        data.getSlides().set(data.getSlides().indexOf(y), x);
        data.getSlides().set(data.getSlides().indexOf(x),temp);
    }
    
    
    class slideUp_Transaction implements jTPS_Transaction{
        Slide slide1;
        Slide slide2;
        public slideUp_Transaction(Slide slide1, Slide slide2){
            this.slide1 = slide1;
            this.slide2 = slide2;
        }
        @Override
        public void doTransaction(){
            swapUp(slide1,slide2);
        }
        @Override
        public void undoTransaction(){
            swapDown(slide1,slide2);
        }   
    }
    
    class title_Transaction implements jTPS_Transaction{
        String title;
        String oriTitle;
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        public title_Transaction(String title){
            this.title = title;
            oriTitle = data.getText();
        }
        @Override
        public void doTransaction(){
            data.setText(title);
            Title.setText(title);
        }
        @Override
        public void undoTransaction(){
            data.setText(oriTitle);
            Title.setText(oriTitle);
        }   
    }
    
    public void slideShow(){
        index = 0;
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        Stage secondStage = new Stage();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String appIcon = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO);
        secondStage.getIcons().add(new Image(appIcon));
        secondStage.setTitle(Title.getText());
        
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(PLAYANDPAUSE_BUTTON_ICON.toString()); 
        Image playPause = new Image(imagePath);
        Button PlayAndPause = new Button();
        PlayAndPause.setGraphic(new ImageView(playPause));
        PlayAndPause.getStyleClass().add(CLASS_EDIT_BUTTON);
        
        String imagePath1 = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(PREVIOUS_BUTTON_ICON.toString()); 
        Image previous = new Image(imagePath1);
        Button previousButton = new Button();
        previousButton.setGraphic(new ImageView(previous));
        previousButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        
        String imagePath2 = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(NEXT_BUTTON_ICON.toString()); 
        Image next = new Image(imagePath2);
        Button nextButton = new Button();
        nextButton.setGraphic(new ImageView(next));
        nextButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        
        final ImageView studentImageView = new ImageView();
        HashMap<String, Image> images = new HashMap<>();
        Label caption = new Label();
        for (int i = 0; i < data.getSlides().size(); i++){
            String fileName = data.getSlides().get(i).getFileName();
            try{
                Image image = loadImage(data.getSlides().get(i).getPath());
                images.put(fileName,image);
            }catch(Exception e){
                System.out.println("ERROR: " + data.getSlides().get(i).getPath());
            }
        }
        
        FlowPane buttonToolbar = new FlowPane();
        buttonToolbar.setAlignment(Pos.BOTTOM_CENTER);
        buttonToolbar.getChildren().add(previousButton);
	buttonToolbar.getChildren().add(PlayAndPause);
	buttonToolbar.getChildren().add(nextButton);
        
        
        Image firstSlide = images.get(data.getSlides().get(index).getFileName());
        caption.setText(data.getSlides().get(index).getCaption());
        studentImageView.setImage(firstSlide);
        studentImageView.setFitHeight(data.getSlides().get(index).getCurrentHeight());
        studentImageView.setFitWidth(data.getSlides().get(index).getCurrentWidth());
        List<String> fontFamilies = javafx.scene.text.Font.getFamilies();
        String randomFontFamily = fontFamilies.get((int)(Math.random()*fontFamilies.size()));
        caption.setFont(new Font(randomFontFamily, 60));
        
        VBox root = new VBox();
        root.setTranslateX(data.getSlides().get(index).getX());
        root.setTranslateY(data.getSlides().get(index).getY());
        root.getChildren().add(studentImageView);
	root.getChildren().add(caption);
        BorderPane Root = new BorderPane();
        Root.setCenter(root);
        Root.setBottom(buttonToolbar);
        
        Scene scene = new Scene(Root, 1000, 700);
	secondStage.setScene(scene);
        
        nextButton.setOnAction(e->{
            increaseIndex();
            Image slide = images.get(data.getSlides().get(index).getFileName());
            studentImageView.setImage(slide);
            caption.setText(data.getSlides().get(index).getCaption());
            studentImageView.setFitHeight(data.getSlides().get(index).getCurrentHeight());
            studentImageView.setFitWidth(data.getSlides().get(index).getCurrentWidth());
            root.setTranslateX(data.getSlides().get(index).getX());
            root.setTranslateY(data.getSlides().get(index).getY());
            caption.setFont(new Font(randomFontFamily, 60));
        });
        previousButton.setOnAction(e->{
            decreaseIndex();
            Image slide = images.get(data.getSlides().get(index).getFileName());
            studentImageView.setImage(slide);
            caption.setText(data.getSlides().get(index).getCaption());
            studentImageView.setFitHeight(data.getSlides().get(index).getCurrentHeight());
            studentImageView.setFitWidth(data.getSlides().get(index).getCurrentWidth());
            root.setTranslateX(data.getSlides().get(index).getX());
            root.setTranslateY(data.getSlides().get(index).getY());
            caption.setFont(new Font(randomFontFamily, 60));
        });
        PlayAndPause.setOnAction(e->{
            Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                if (isPlayed == true){
                    isPlayed = false;
                    //nextButton.setDisable(false);
                    //previousButton.setDisable(false);
                }else{
                    isPlayed = true;
                    //nextButton.setDisable(true);
                    //previousButton.setDisable(true);
                }
                while(isPlayed){
                    increaseIndex();
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            Image slide = images.get(data.getSlides().get(index).getFileName());
                            studentImageView.setImage(slide);
                            caption.setText(data.getSlides().get(index).getCaption());
                            studentImageView.setFitHeight(data.getSlides().get(index).getCurrentHeight());
                            studentImageView.setFitWidth(data.getSlides().get(index).getCurrentWidth());
                            root.setTranslateX(data.getSlides().get(index).getX());
                            root.setTranslateY(data.getSlides().get(index).getY());
                            caption.setFont(new Font(randomFontFamily, 60));
			}
                    });
                    try {
			Thread.sleep(2000);
                    } catch (InterruptedException ie) {
			ie.printStackTrace();
                    }
                }
                return null;            
            }
        };
            Thread thread = new Thread(task);
            thread.start();
        });
        
        secondStage.initModality(Modality.APPLICATION_MODAL);
        secondStage.showAndWait();
        
    }
    
    
    public Image loadImage(String imagePath) throws MalformedURLException {
	File file = new File(imagePath);
	URL fileURL = file.toURI().toURL();
	Image image = new Image(fileURL.toExternalForm());
	return image;
    }
    int index;
    boolean isPlayed = false;
    public void increaseIndex(){
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        if (index == data.getSlides().size()-1)
            index = 0;
        else
            index++;
    }
    public void decreaseIndex(){
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        if (index == 0)
            index = data.getSlides().size()-1;
        else
            index--;
    }
}
