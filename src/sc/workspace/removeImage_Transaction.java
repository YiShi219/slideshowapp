package sc.workspace;

import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import sc.data.Slide;

public class removeImage_Transaction implements jTPS_Transaction{
    ObservableList<Slide> slides;
    String path;
    Slide slideDeleted = null;
    int index = 0;
    public removeImage_Transaction(ObservableList<Slide> slides, String path){
        this.slides = slides;
        this.path = path;
    }
    @Override
    public void doTransaction(){
        for (int i = 0; i < slides.size(); i++){
            if (slides.get(i).getPath().equals(path)){
                slideDeleted = slides.remove(i);
                index = i;
            }
        }
    };
    @Override
    public void undoTransaction(){
        slides.add(index,slideDeleted);
    };
}
