package sc.data;

import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import java.util.List;
import javafx.collections.FXCollections;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import sc.SlideshowCreatorApp;
import sc.addAll_Transaction;
import sc.addImage_Transaction;
import sc.workspace.removeImage_Transaction;

/**
 * This is the data component for SlideshowCreatorApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 */
public class SlideshowCreatorData implements AppDataComponent {
    jTPS jTPS = new jTPS();
    public boolean added = false;

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    SlideshowCreatorApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<Slide> slides;
    
    String SlideShowTitle = "";
    
    public void setText(String txt){
        SlideShowTitle = txt;
    }
    public String getText(){
        return SlideShowTitle;
    }
    public boolean noSlide(){
        return slides.isEmpty();
    }

    /**
     * This constructor will setup the required data structures for use.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public SlideshowCreatorData(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        
        // MAKE THE SLIDES MODEL
        slides = FXCollections.observableArrayList();
    }
    
    // ACCESSOR METHOD
    public ObservableList<Slide> getSlides() {
        return slides;
    }
    
    
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {
        slides.clear();
        SlideShowTitle = "";
    }

    // FOR ADDING A SLIDE WHEN THERE ISN'T A CUSTOM SIZE
    public void addSlide(String fileName, String path, String caption, int originalWidth, int originalHeight) {
        for (int i = 0; i < slides.size(); i++){
            if (slides.get(i).getFileName().equals(fileName) || slides.get(i).getPath().equals(path)){
                added = false;
                return;
            }
        }
        added = true;
        addImage_Transaction transaction = new addImage_Transaction(slides,fileName,path,caption,originalWidth,originalHeight);
        jTPS.addTransaction(transaction);
        app.getWorkspaceComponent().updateRedoButton();
    }

    // FOR ADDING A SLIDE WITH A CUSTOM SIZE
    public void addSlide(String fileName, String path, String caption, int originalWidth, int originalHeight, int currentWidth, int currentHeight) {
        Slide slideToAdd = new Slide(fileName, path, caption, originalWidth, originalHeight);
        slideToAdd.setCurrentWidth(currentWidth);
        slideToAdd.setCurrentHeight(currentHeight);
        slides.add(slideToAdd);
    }
    public void addSlide(List<Slide> slide){
        for (int i = 0; i < slide.size(); i++){
            for (int j = 0; j < slides.size(); j++){
                if (slide.get(i).getPath().equals(slides.get(j).getPath()) || slide.get(i).getFileName().equals(slides.get(j).getFileName()))
                    slide.remove(i);
            }
        }
        if (slide.size() != 0){
            added = true;
            addAll_Transaction transaction = new addAll_Transaction(slide, slides);
            jTPS.addTransaction(transaction);
        }
    }
    
    public void addSlide(String fileName, String path, String caption, int originalWidth,
            int originalHeight, int currentWidth, int currentHeight, int xPos, int yPos){
        Slide slideToAdd = new Slide(fileName, path, caption, originalWidth, originalHeight);
        slideToAdd.setCurrentWidth(currentWidth);
        slideToAdd.setCurrentHeight(currentHeight);
        slideToAdd.setX(xPos);
        slideToAdd.setY(yPos);
        slides.add(slideToAdd);
    }
    
    // FOR REMOVING A SLIDE SELECTED
    public void removeSlide(String path){
        removeImage_Transaction transaction = new removeImage_Transaction(slides,path);
        jTPS.addTransaction(transaction);
        app.getWorkspaceComponent().updateRedoButton();
    }
    
}