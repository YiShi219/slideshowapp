
package sc;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import sc.data.Slide;

public class addAll_Transaction implements jTPS_Transaction{
    List<Slide> list;
    ObservableList<Slide> slides;
    int size;
    public addAll_Transaction(List<Slide> list, ObservableList<Slide> slides){
        this.list = new ArrayList<>();
        //this.list = list;
        for (int i = 0; i < list.size(); i++){
            this.list.add(list.get(i));
        }
        this.slides = slides;
        size = list.size();
    }
    
    @Override
    public void doTransaction(){
        for (int i = 0; i < list.size(); i++){
            //Slide slide = list.get(i);
            slides.add(list.get(i));
        }
    }
    @Override
    public void undoTransaction(){
        slides.remove(slides.size()-size,slides.size());
    }
}
