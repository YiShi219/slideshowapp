
package sc;

import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import sc.data.Slide;

public class addImage_Transaction implements jTPS_Transaction{
        Slide slide;
        ObservableList<Slide> slides;
        String fileName; String path; String caption; int originalWidth; int originalHeight;
        public addImage_Transaction(ObservableList<Slide> slides, String fileName, String path, String caption, int originalWidth, int originalHeight){
            this.slides = slides;
            this.fileName = fileName;
            this.path = path;
            this.caption = caption;
            this.originalHeight = originalHeight;
            this.originalWidth = originalWidth;
            slide = new Slide(fileName, path, caption, originalWidth, originalHeight);
        }
        @Override
        public void doTransaction(){
            slides.add(slide);
           
        };
        @Override
        public void undoTransaction(){
            slides.remove(slide);
        }
}
